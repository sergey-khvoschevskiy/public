"""
class TableList:
   def __init__(self,rss_uri,news_uri,published_date,title,summary=""):
      self.rss_uri=rss_uri
      self.news_uri=news_uri
      self.published_date=published_date
      self.title=title
      self.summary=summary

class Dictionary:
   '''
   {"key1":[["key1","a","b","c"],["key1","a2","b2","c","2"]],"key2":[["key2","a","b","c"],["key2","a2","b2","c2"]],"key3":[["key3","a","b","c"],["key3","a2","b2","c2"]]}
   '''
   def __init__(self,key='',table=[]):
      self.key=key
      self.table=table

class Table:
   '''
   [[key1,a,b,c],[key1,a2,b2,c,2],[key2,a,b,c],[key2,a2,b2,c2],[key3,a,b,c],[key3,a2,b2,c2]]
   '''
   def __init__(self,key,array):
      self.key=key
      self.array=array
"""
import csv
def ReadCsv(filecsv):
   with open(str(filecsv), 'r') as fd:
       #writer = csv.writer(fd, delimiter=',')
       reader = csv.reader(fd, delimiter=',', quotechar='"')
       # writer.writerow(["your", "header", "foo"])  # write header
       return [row for row in reader]

def DictionaryToCsv(Dictionary,filecsv):
   with open(str(filecsv), 'w') as fd:
      writer = csv.writer(fd, delimiter=',')
      #reader = csv.reader(fd, delimiter=',', quotechar='"')
      try:
         for key in Dictionary.keys():
            for j in range(len(Dictionary[key])):
               writer.writerow([Dictionary[key][j][0], Dictionary[key][j][1],Dictionary[key][j][2],Dictionary[key][j][3],Dictionary[key][j][4]])  # write header
      except Exception as e:
         print(e)

def WriteTableToCsv(Table,filecsv,mode):
   with open(str(filecsv), str(mode)) as fd:
      writer = csv.writer(fd, delimiter=',')
      try:
         for i in range(len(Table)):
            writer.writerow([Table[i][j] for j in range(len(Table[0]))])
            #writer.writerow([Table[i][0], Table[i][1],Table[i][2],Table[i][3],Table[i][4]])  # write header
      except Exception as e:
         print(e)
         
def AddSetToCsv(SEt,filecsv):
   with open(str(filecsv), 'a') as fd:
      writer = csv.writer(fd, delimiter=',')
      try:
         for i in range(len(SEt)):
            row=(list(SEt))[i].split('\', \'')
            writer.writerow([row[0], row[1],row[2],row[3],row[4]])  # write header
      except Exception as e:
         print(e)

def TemplateDictionary(TableUniq):
   DictionaryTemplate={}
   for i in range(len(TableUniq[1::])):#первая строка название колонок - поэтому пропускаем
      DictionaryTemplate.update({TableUniq[1::][i][0]:[]}) #генерируется общий обновляемый словарь SuperFeed
   return DictionaryTemplate

def TableToDictionary(Table,Dictionary):
   temp_lst=[]
   #добавляет таблицу в словарь, возможны дубли!!!!!
   for i in range(len(Table)):
      if isinstance(Dictionary.get(Table[i][0]),type(None)):
         Dictionary.update({str(Table[i][0]):[]})
         temp_lst=Dictionary.get(Table[i][0])
         temp_lst.append(Table[i])
         temp_dict={str(Table[i][0]):temp_lst}
         temp_dict.update(Dictionary)
      else:
         temp_lst=Dictionary.get(Table[i][0])
         temp_lst.append(Table[i])
         temp_dict={str(Table[i][0]):temp_lst}
         temp_dict.update(Dictionary)
   return Dictionary
   
def DictionaryToTable(Dictionary,Table):
   Table = [] if (len(Table)==0 ) else Table
   for key in Dictionary.keys():
      for lst in Dictionary[key]:
         Table.append([lst[0], lst[1],lst[2],lst[3],lst[4]])
   return Table

import feedparser
import datetime
def Requester(ConditionsTable, timeignoreflag=0):
   Dictionary={}
   for i in range(len(ConditionsTable[1::])):
      uri=ConditionsTable[1::][i][0]
      #проверяется надо ли делать запрос
      if (datetime.datetime.utcnow().timestamp() > datetime.datetime.strptime(ConditionsTable[1::][i][2],'%a %b %d %H:%M:%S %Y').timestamp() + int(ConditionsTable[1::][i][1])) or bool(timeignoreflag):
         #print(datetime.datetime.strptime(Source[1::][i][2],'%a %b %d %H:%M:%S %Y'),"old reuest()")
         #s=datetime.datetime.strptime(Feed[4][2], '%a, %d %b %Y %H:%M:%S %z')   
         #s.utcnow().timestamp()
         Feed=[] #это список для каждой ленты отдельно
         try:
            NewsFeed=feedparser.parse(str(uri))
            ConditionsTable[1::][i][2]=datetime.datetime.utcnow().ctime()
            for i in range(len(NewsFeed.entries)):
            #if not isinstance( NewsFeed.get('bozo_exception'),type(None)):
               if not isinstance(NewsFeed.entries[i].get("summary"),type(None)):
                  Feed.append((NewsFeed.href, NewsFeed.entries[i].link,NewsFeed.entries[i].published,NewsFeed.entries[i].title,NewsFeed.entries[i].summary))
               else:
                  Feed.append((NewsFeed.href, NewsFeed.entries[i].link,NewsFeed.entries[i].published,NewsFeed.entries[i].title,""))
         except AttributeError as e:
            print("AttributeError:",e)      
         except Exception as e:
            print("Probably error requested rss",uri)
            print("Exception:",e)
         finally :
            Dictionary[uri]=Feed
            print("in",uri,len(Dictionary[uri]),"records")
            if not isinstance( NewsFeed.get('bozo_exception'),type(None)): 
               print(NewsFeed.get('bozo_exception'))
            print("================================================================================================================================")
            #if Feed.get('bozo_exception') not None
            #print(Feed.get('bozo_exception'))
   return Dictionary


def SelectNew(DicNew,DicOld):
   Table=[]
   for key in DicNew.keys():
      for j in range(len(DicNew[key])):
         if (DicNew[key][j][1] and DicNew[key][j][2]) in DicOld[key][0] :
            pass#если появилось значение это значит, что все остальные записи должны совпадать
         else:
            Table.append([DicNew[key][j][0], DicNew[key][j][1],DicNew[key][j][2],DicNew[key][j][3],DicNew[key][j][4]])  # write header
   return Table

def DiffTable(TableNew,TableOld):
    SetOld=set([str(lst) for lst in TableOld ])
    SetNew=set([str(lst) for lst in TableNew ])
    return SetNew.difference(SetOld)

from time import sleep
if __name__=='main':
   def main():
      OldSuperFeed={}
      OldSuperFeed=TableToDictionary(ReadCsv("oldresult.csv"),{})
      Source=ReadCsv("source.csv")
      while True:
         try:
            SuperFeed=Requester(Source)
            NewFeed=DictionaryToTable(SuperFeed,[])
            OldFeed=DictionaryToTable(OldSuperFeed,[])
            New=DiffTable(NewFeed,OldFeed)
            if len(New) > 10 :
               AddSetToCsv(list(New),"result.csv")
               OldSuperFeed=TableToDictionary(New,{})
            WriteTableToCsv(Source,"source.csv","w")   
         except Exception as e:
            print("Exception:",e,"occured")
         sleep(3620)
